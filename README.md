# Foncedalle

**Projet chef d'oeuvre Simplon**

*****

Foncedalle est une application e-commerce réalisée dans le cadre de ma formation de développeur web et web mobile. 

**Technologies :**

- Symfony + EasyAdmin
- React + React Awesome + Axios
- Twig
- Sass

## Init

*****

**Install packages**

```shell
composer install
```

```shell
npm install
```

**Create databse**

```shell
cp .env .env.local
```

Edit .env.local 

```shell
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL=mysql://<db_user>:<db_password>@127.0.0.1:<3306>/<db_name>?serverVersion=<10.5.6-MariaDB>
###< doctrine/doctrine-bundle ###
```

```shell
bin/console doctrine:database:create
```

```shell
bin/console make:migration
```

```shell
bin/console doctrine:migrations:migrate
```

## Build

******

```shell
npm run watch
```

## Server

******

```shell
symfony:serve
```
local access : http://127.0.0.1:8000

## Admin

Create user account

http://127.0.0.1:8000/registration

In your database edit admin's entity "roles" with ["ROLE_SUPER_ADMIN"], then login

http://127.0.0.1:8000/login

You should be able to access to

http://127.0.0.1:8000/admin
