<?php

namespace Entity;

use App\Entity\Family;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class FamilyTest extends KernelTestCase
{
    public function getFamily(): Family
    {
        return (new Family())
            ->setName('Valid name')
            ->setUpdated(new \DateTime())
            ;
    }

    public function assertHasErrors(Family $family, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($family);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error)
        {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidFamilyEntity()
    {
        $this->assertHasErrors($this->getFamily(), 0);
    }

    public function testInvalidFamilyEntity()
    {
        // Invalid character, less than 3 characters, more than 15 characters
        $this->assertHasErrors($this->getFamily()->setName('Invalid-name'), 1);
        $this->assertHasErrors($this->getFamily()->setName('12'), 1);
        $this->assertHasErrors($this->getFamily()->setName('1234567890123456'), 1);
    }
}
