<?php

namespace Entity;

use App\Entity\Family;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class ProductTest extends KernelTestCase
{
    public function getEntity() :Product
    {
        $family = new Family();
        $family->getId();

        return (new Product())
            ->setName('Valid name')
            ->setDescription('Valid descr')
            ->setPrice('2,50')
            ->setUpdated(new \DateTime())
            ->setFamily($family)
            ;
    }

    public function assertHasErrors(Product $product, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($product);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error)
        {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidProductEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidProductEntity()
    {
        // Invalid character, less than 3 characters, more than 15 characters
        $this->assertHasErrors($this->getEntity()->setName('Invalid-name'), 1);
        $this->assertHasErrors($this->getEntity()->setName('12'), 1);
        $this->assertHasErrors($this->getEntity()->setName('1234567890123456'), 1);
        // Invalid character, less than 3 characters, more than 15 characters
        $this->assertHasErrors($this->getEntity()->setDescription('Invalid-descr'), 1);
        $this->assertHasErrors($this->getEntity()->setDescription('12'), 1);
        $this->assertHasErrors($this->getEntity()->setDescription('1234567890123456'), 1);
        // Invalid characters
        $this->assertHasErrors($this->getEntity()->setPrice('2.50'), 1);
        $this->assertHasErrors($this->getEntity()->setPrice('a.50'), 1);
        // Invalid relation
        $this->assertHasErrors($this->getEntity()->setFamily(null), 1);
    }
}
