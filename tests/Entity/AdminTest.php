<?php

namespace App\Tests\Entity;

use App\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class AdminTest extends KernelTestCase
{
    public function getAdmin(): Admin
    {
        return (new Admin())
            ->setUsername('Valid name')
            ->setPassword('123456')
            ;
    }

    public function assertHasErrors(Admin $admin, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($admin);
        $messages = [];
        /**
         * @var ConstraintViolation $error
         */
        foreach ($errors as $error)
        {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidAdminEntity()
    {
        $this->assertHasErrors($this->getAdmin(), 0);
    }

    public function testInvalidAdminEntity()
    {
        // Invalid character, less than 3 characters, more than 15 characters
        $this->assertHasErrors($this->getAdmin()->setUserName('Invalid-name'), 1);
        $this->assertHasErrors($this->getAdmin()->setUserName('12'), 1);
        $this->assertHasErrors($this->getAdmin()->setUserName('1234567890123456'), 1);
        // Invalid character, less than 3 characters, more than 15 characters
        $this->assertHasErrors($this->getAdmin()->setPassword('Invalid-pass'));
        $this->assertHasErrors($this->getAdmin()->setPassword('12'));
        $this->assertHasErrors($this->getAdmin()->setPassword('1234567890123456-pass'));
    }
}
