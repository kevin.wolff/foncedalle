import React, { Fragment, useState, useEffect } from 'react'
import ShoppingContext from './ShoppingContext'
import Fetcher from './Fetcher'
import HandleResize from './HandleResize'
import SearchBar from './components/search-bar/Search-bar'
import SideMenu from './components/side-menu/Side-menu'
import Products from './components/products/Products'
import Pagination from './components/pagination/Pagination'
import Cart from './components/cart/Cart'

export default function Layout() {

    const [ familiesData ] = Fetcher('/api/families')
    const [ urlProducts, setUrlProducts ] = useState('/api/products')
    const [ productsData ] = Fetcher(urlProducts)
    const [ family, setFamily ] = useState('')
    const [ query, setQuery ] = useState('')
    const [ order, setOrder ] = useState('')
    const localData = localStorage.getItem('cart')
    const [ productsInCart, setProductsInCart ] = useState( () => { return localData ? JSON.parse(localData) : [] })
    const [ cartInfos, setCartInfo ] = useState({ productsQty: '', totalTTC: '', totalHT: '', totalTax: '' })
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ productsPerPage, setProductsPerPage ] = useState(Number)
    const [ cartViewState, setCartViewState ] = useState(false)
    const [ orderFormViewState, setOrderFormViewState ] = useState(false)

    // Commentaire dans shoppingContext.js
    const shoppingContextValue = {
        urlProducts,
        updateUrlProducts: setUrlProducts,
        familiesData,
        productsData,
        family,
        query,
        fetchWithFilter: fetchWithFilter,
        productsInCart,
        cartInfos,
        addProductInCart: addProductInCart,
        removeProductFromCart: removeProductFromCart,
        clearProductFromCart: setProductsInCart,
        productsPerPage,
        currentPage,
        changePage: setCurrentPage,
        cartCompView: cartViewState,
        updateCartCompView: setCartViewState,
        orderFormCompView: orderFormViewState,
        updateOrderFormCompView: setOrderFormViewState
    }

    // Lorsque la page est chargée lance la fonction handleResize pour définir le nmbr de produits par pages
    window.onload = function() { setProductsPerPage(HandleResize()) }

    // Event listener de la page, execute handleResize si on modifie la taille de la page
    useEffect(() => {
        window.addEventListener('resize', () => { setProductsPerPage(HandleResize()) })
        return() => {
            window.removeEventListener('resize', () => { setProductsPerPage(HandleResize()) })
        }
    })

    // Fctn pour fetch produits avec requêtes
    function fetchWithFilter(type, request)  {
        const searchBar = document.querySelector('#search-bar')
        if (type === 'query') {
            // Query en cours et nouvelle query, remplace ancienne valeur par nouvelle
            if (query && request) {
                const currentQuerry = query
                setQuery(request)
                setUrlProducts(urlProducts.replace(`query=${currentQuerry}`, `query=${request}`))
            } else if (request) { // Pas de query en cours
                setQuery(request)
                // Ajoute la query a l'url produits
                if (urlProducts.includes('family') || urlProducts.includes('order')) {
                    setUrlProducts(urlProducts + `&query=${request}`)
                } else {
                    setUrlProducts(urlProducts + `?query=${request}`)
                }
            } else { // Nouvelle query vide
                setUrlProducts('/api/products')
                setFamily('')
                setQuery('')
                setOrder('')
            }

        }
        else if (type === 'order') {
            // Ordre en cours et nouvel ordre, remplace ancienne valeur par nouvelle
            if (order && request) {
                const currentOrder = order
                setOrder(request)
                setUrlProducts(urlProducts.replace(`order=${currentOrder}`, `order=${request}`))
            } else if (request) { // Pas d'ordre en cours
                setOrder(request)
                // Ajoute la query a l'url produits
                if (urlProducts.includes('family') || urlProducts.includes('query')) {
                    setUrlProducts(urlProducts + `&order=${request}`)
                } else {
                    setUrlProducts(urlProducts + `?order=${request}`)
                }
            }
        }
        else if (type === 'family') {
            setFamily(request)
            // Ecrase l'url actuelle pour une recherche par famille
            if (request) {
                setUrlProducts(`/api/products?family=${request}`)
            } else { // Ecrase l'url actuelle pour afficher tous les produits
                setUrlProducts('/api/products')
                setQuery('')
                setOrder('')
            }
            // Reset les filtres
            setQuery('')
            setOrder('')
        }
        // Reset à la première page et ferme le composant panier d'achat
        setCurrentPage(1)
        setCartViewState(false)
        if (searchBar.classList.contains('toggle-mobile')) {
            searchBar.classList.remove('toggle-mobile')
        }
    }

    // Lorsque le hook productsInCart est modifié, localStorage le nouveau tableau de produit séléctionnés
    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(productsInCart))
    }, [ productsInCart ])

    // Si la valeur de productsInCart est modifié, rafraichis les datas de cartInfos
    useEffect(() => {
        setCartInfo({
            productsQty: productsInCart.reduce((x, current) => x + current.qty, 0),
            totalTTC: (productsInCart.reduce((x, current) => x + parseFloat(current.price) * current.qty, 0)).toFixed(2),
            totalHT: ((productsInCart.reduce((x, current) => x + parseFloat(current.price) * current.qty, 0)).toFixed(2) * 0.80).toFixed(2),
            totalTax: ((productsInCart.reduce((x, current) => x + parseFloat(current.price) * current.qty, 0)).toFixed(2) * 0.20).toFixed(2)
        })
    }, [ productsInCart ])

    // Fctn pour ajouter un produit au panier d'achat - ShoppingContext
    function addProductInCart(product) {
        const exist = productsInCart.find((x) => x.id === product.id)
        if (exist) {
            setProductsInCart(productsInCart.map((x) => x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x))
        } else {
            setProductsInCart([...productsInCart, { ...product, qty: 1 }])
        }
    }

    // Fctn pour enlever un produit au panier d'achat - ShoppingContext
    function removeProductFromCart(product) {
        const exist = productsInCart.find((x) => x.id === product.id)
        if (exist.qty === 1) {
            setProductsInCart(productsInCart.filter((x) => x.id !== product.id))
        } else {
            setProductsInCart(productsInCart.map((x) => x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x))
        }
    }

    // Si les fetch familles et produits sont en loading ou si l'un des deux à une erreur
    if ( productsData.loading && familiesData.loading || productsData.error || familiesData.error ) {
        const textToDisplay = productsData.loading || familiesData.loading === true ? 'Chargement' : 'Une erreur est survenue'
        return (
            <ShoppingContext.Provider value={ shoppingContextValue }>
                <Fragment>
                    <SearchBar />
                    <div className='loading-animation layout'>
                        <p>{ textToDisplay }</p>
                        <div className='wrapper-loading-dots'>
                            <span className='loading-dot'/>
                            <span className='loading-dot'/>
                            <span className='loading-dot'/>
                        </div>
                    </div>
                </Fragment>
            </ShoppingContext.Provider>
        )
    }

    // Si l'utilisateur consulte sont panier d'achat
    if (cartViewState) {
        return (
            <ShoppingContext.Provider value={ shoppingContextValue }>
                <Fragment>
                    <SearchBar />
                    <SideMenu />
                    <Cart />
                </Fragment>
            </ShoppingContext.Provider>
        )
    }

    return (
        <ShoppingContext.Provider value={ shoppingContextValue }>
            <Fragment>
                <SearchBar />
                <SideMenu />
                <Products />
                <Pagination />
            </Fragment>
        </ShoppingContext.Provider>
    )
}
