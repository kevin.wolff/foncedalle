import { useState, useEffect } from 'react'
import Axios from 'axios'

const Fetcher = url => {
    const [ data, setData ] = useState([])
    const [ loading, setLoading ] = useState(false)
    const [ error, setError ] = useState(false)

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true)
            try {
                const res = await Axios.get(url)
                setData(res.data)
                setLoading(false)
            }
            catch (err) {
                setError(true)
                console.log(`Error: ${ err }`)
            }
        }
        fetchData()
    }, [ url ])

    return [{ data, loading, error }]
}

export default Fetcher
