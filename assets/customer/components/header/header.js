function header() {

    const header = document.querySelector('#header')
    const mobileMenu = document.querySelector('.header-burger-menu')

    // Bouton pour afficher/masquer le menu en display mobile
    mobileMenu.addEventListener('click', () => {
        if (header.classList.contains('toggle-mobile')) {
            header.classList.remove('toggle-mobile')
        } else {
            header.classList.add('toggle-mobile')
        }
    })
}

export { header }
