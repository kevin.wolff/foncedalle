import React, { useState, useEffect, useContext } from 'react'
import ShoppingContext from '../../ShoppingContext'
import Axios from 'axios'
import Recaptcha from 'react-recaptcha'

export default function OrderForm({ requestResponse }) {

    const [ name, setName ] = useState('')
    const [ surname, setSurname ] = useState('')
    const [ email, setEmail ] = useState('')
    const [ phoneNumber, setPhoneNumber ] = useState('')
    const [ getMethod, setGetMethod ] = useState('boutique')
    const [ address, setAddress ] = useState('')
    const [ zipCode, setZipCode ] = useState('')
    const [ getDateRaw, setGetDateRaw ] = useState('')
    const [ getDateFormated, setGetDateFormated ] = useState('')
    const [ rgpdAgree, setRgpdAgree ] = useState(false)
    const [ verified, setVerified ] = useState(false)

    // Commentaire dans shoppingContext.js
    const {
        productsInCart,
        cartInfos,
        clearProductFromCart
    } = useContext(ShoppingContext)

    // Formatte la valeur de l'input date '2021-01-31' > '31-01-2021'
    useEffect(() => {
        const year = getDateRaw.slice(0,4)
        const month = getDateRaw.slice(5,7)
        const day = getDateRaw.slice(8,10)
        setGetDateFormated(getDateRaw === '' ? '' : day + '-' + month + '-' + year)
    }, [ getDateRaw ])

    // Instance du captcha
    let recaptchaInstance

    // Set hook verified à true lorsque l'utilisateur a validé le captcha
    const verifyCallback = () => {
        setVerified(true)
    }

    // Fctn pour reset le captcha et le hook verified
    const resetRecaptcha = () => {
        recaptchaInstance.reset()
        setVerified(false)
    }

    // Fctn pour créer une nouvelle commande
    const createOrder = async (e) => {
        e.preventDefault()
        try {
            const res = await Axios.post('/api/order', {
                // Order
                ammount: cartInfos.totalTTC,
                getMethod: getMethod,
                getDate: getDateFormated,
                status: '0',
                // Customer
                name: name,
                surname: surname,
                email: email,
                phoneNumber: phoneNumber,
                address: address,
                zipCode: zipCode,
                rgpdAgree: rgpdAgree,
                // OrderDetails
                products: productsInCart,
            })
            requestResponse(201, '')
            clearProductFromCart([])
        }
        catch (err) {
            console.log(err)
            requestResponse(1, `${err}`)
        }
    }

    return (
        <form onSubmit={ createOrder } id='order-form'>
            <h2>Renseignements</h2>
            <div className='form-wrapper name-surname'>
                <div className='form-name'>
                    <label htmlFor='name'>Nom</label>
                    <input required onChange={() => { setName(event.target.value) }} type="text" id='name' pattern='[a-z0-9A-Zéèàê\- ]*' maxLength='20' minLength='3' title='Lettres et chiffres uniquement, minimum 3 maximum 20'/>
                </div>
                <div className='form-surname'>
                    <label htmlFor='surname'>Prénom</label>
                    <input required onChange={() => { setSurname(event.target.value) }} type="text" id='surname' pattern='[a-z0-9A-Zéèàê\- ]*' maxLength='20' minLength='3' title='Lettres et chiffres uniquement, minimum 3 maximum 20'/>
                </div>
            </div>
            <div className='form-mail'>
                <label htmlFor='email'>Adresse mail</label>
                <input required onChange={() => { setEmail(event.target.value) }} type='email' id='email' pattern='[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+' title='Ex: foncedalle@gmail.com'/>
            </div>
            <div className='form-number'>
                <label htmlFor='phoneNumber'>Numéro de téléphone</label>
                <input required onChange={() => { setPhoneNumber(event.target.value) }} type='tel' id='phoneNumber' pattern='^[0-9]{1,10}$' title='Ex: 0611007474, 10 nombres maximum'/>
            </div>
            <h2 className='getOrder'>Récupérer votre commande</h2>
            <a href='#' className='getOrderInfoLink'>Infos sur pick up et livraison</a>
            <div className='form-wrapper get-methods'>
                <div className='form-pickup'>
                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 128 128'><g><path d='M123.443,41.844V30.76c0-.024-.006-.046-.007-.069a1.528,1.528,0,0,0-.019-.19c-.008-.054-.017-.108-.03-.161s-.031-.107-.05-.16a1.737,1.737,0,0,0-.07-.173c-.011-.021-.016-.044-.027-.066L110.279,5.487a1.75,1.75,0,0,0-1.546-.93H19.267a1.75,1.75,0,0,0-1.546.93L4.76,29.941c-.011.022-.016.045-.027.066a1.737,1.737,0,0,0-.07.173c-.019.053-.036.106-.05.16s-.022.107-.03.161a1.528,1.528,0,0,0-.019.19c0,.023-.007.045-.007.069V41.844A13.292,13.292,0,0,0,12.1,53.806V110.2H6.307a1.75,1.75,0,0,0-1.75,1.75v9.744a1.749,1.749,0,0,0,1.75,1.75H121.693a1.749,1.749,0,0,0,1.75-1.75v-9.744a1.75,1.75,0,0,0-1.75-1.75h-5.788V53.806A13.292,13.292,0,0,0,123.443,41.844Zm-3.5,0a9.789,9.789,0,1,1-19.577,0V32.51h19.577ZM31.134,32.51H50.711v9.334a9.789,9.789,0,1,1-19.577,0ZM52.993,8.057,50.879,29.01H31.8L38.678,8.057Zm18.5,0L73.6,29.01H54.4L56.511,8.057Zm17.833,0L96.2,29.01H77.121L75.007,8.057ZM54.211,32.51H73.789v9.334a9.79,9.79,0,0,1-18.545,4.381,9.68,9.68,0,0,1-1.033-4.381Zm23.078,0H96.866v9.334a9.79,9.79,0,0,1-18.586,4.3,9.685,9.685,0,0,1-.991-4.3Zm41.5-3.5h-18.9L93.006,8.057H107.68ZM20.32,8.057H34.994L28.116,29.01H9.215ZM8.057,41.844V32.51H27.634v9.334a9.789,9.789,0,1,1-19.577,0Zm111.886,78.1H8.057V113.7H119.943ZM55.366,110.2H29V64.541H55.366Zm57.039,0H58.866V62.791a1.75,1.75,0,0,0-1.75-1.75H27.252a1.75,1.75,0,0,0-1.75,1.75V110.2H15.6V54.928a13.172,13.172,0,0,0,13.789-6.5,13.284,13.284,0,0,0,23.074.005,13.289,13.289,0,0,0,23.078,0,13.289,13.289,0,0,0,23.08-.005,13.172,13.172,0,0,0,13.789,6.5Z'/><path d="M100.748,61.041H67.54a1.749,1.749,0,0,0-1.75,1.75v23.26a1.749,1.749,0,0,0,1.75,1.75h33.208a1.75,1.75,0,0,0,1.75-1.75V62.791A1.75,1.75,0,0,0,100.748,61.041ZM99,84.3H69.29V64.541H99Z"/></g></svg>
                    <div>
                        <label htmlFor='pickup'>En boutique</label>
                        <input defaultChecked onChange={() => { setGetMethod(event.target.value) }} type='radio' id='pickup' name='gettingType' value='boutique'/>
                    </div>
                </div>
                <div className='form-delivery'>
                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 64 64'><g><path d='M49,26a12.933,12.933,0,0,0-4.246.726L40.525,17H46.5a1.5,1.5,0,0,1,0,3H44v2h2.5a3.5,3.5,0,0,0,0-7H39a1,1,0,0,0-.917,1.4l2,4.6H23.33l-1.173-3.665,3.364-1.122A2.162,2.162,0,0,0,24.838,12H17.414a2.414,2.414,0,0,0-1.707,4.121l.569.569a4.4,4.4,0,0,0,3.969,1.23l1.074,3.356a.977.977,0,0,0-.2.256l-2.938,5.55a13,13,0,1,0,7.066,16.245A3.986,3.986,0,0,0,28.8,45.98l.751,2.253a1,1,0,0,0,.925.684l3.5.083.046-2-2.8-.066-.456-1.366a3.95,3.95,0,0,0,1.3-6.1L42.081,25.6l.837,1.924A12.987,12.987,0,1,0,49,26ZM20.22,15.873a2.493,2.493,0,0,1-2.53-.6l-.569-.569A.414.414,0,0,1,17.414,14h7.424a.162.162,0,0,1,.051.316ZM13,50a11,11,0,1,1,4.241-21.148l-5.125,9.68a1,1,0,0,0,.7,1.45L23.574,42A11.012,11.012,0,0,1,13,50Zm10.947-9.962-9.427-1.77L19.005,29.8A11,11,0,0,1,24,39C24,39.35,23.98,39.7,23.947,40.038Zm2.032-.631c.005-.136.021-.27.021-.407a12.991,12.991,0,0,0-6.059-10.973l2.205-4.166,4.726,14.765A3.986,3.986,0,0,0,25.979,39.407Zm4.152,4.24L29.448,41.6l-1.9.632.506,1.52a1.992,1.992,0,1,1,2.073-.106Zm.325-5.363A3.966,3.966,0,0,0,29,38c-.076,0-.147.018-.222.022L23.97,23H40.953l.2.463ZM49,50a11,11,0,0,1-5.283-20.644L48.083,39.4l1.834-.8-4.365-10.04A11,11,0,1,1,49,50Z'/></g></svg>
                    <div>
                        <label htmlFor='delivery'>Livraison</label>
                        <input onChange={() => { setGetMethod(event.target.value) }} type='radio' id='delivery' name='gettingType' value='livraison'/>
                    </div>
                </div>
            </div>
            { getMethod === 'livraison' &&
            <div className='form-wrapper getInfos'>
                <div className='form-address'>
                    <label htmlFor='address'>Adresse</label>
                    <input required onChange={() => { setAddress(event.target.value) }} type='text' id='address' pattern='[a-z0-9A-Zéèàê\-\, ]*' maxLength='50' minLength='5' title='Lettres et chiffres uniquement,ex: 130 Cours Berriat, app 9'/>
                </div>
                <div className='form-zipCode'>
                    <label htmlFor='zipCode'>Code postal</label>
                    <input required onChange={() => { setZipCode(event.target.value) }} type='text' id='zipCode' pattern='^[0-9]{1,5}$' title='Ex: 38000, 5 nombres maximum'/>
                </div>
            </div>
            }
            <div className='form-date'>
                <label htmlFor='date'>Date</label>
                <input required onChange={() => { setGetDateRaw(event.target.value) }} type='date' id='date'/>
            </div>
            <div className='form-rgpd'>
                <input required onChange={() => { setRgpdAgree(true) }} type='checkbox' id='rgpd'/>
                <label htmlFor='rgpd'>Vous acceptez notre <a href='#'>politique de confidentialité</a></label>
            </div>
            <div className='form-captcha'>
                <Recaptcha
                    sitekey='6LfTHk8aAAAAAFpvTDfGpnIl1tYfCrbpbNj8MWVs'
                    render='explicit'
                    hl='fr'
                    ref={ e => recaptchaInstance = e }
                    verifyCallback={ verifyCallback }
                />
                <p onClick={ resetRecaptcha }>Réinitialiser le captcha</p>
            </div>
            { verified &&
            <button type='submit' className='btn'>
                <p>Commander</p>
            </button>}
            { !verified &&
            <div className='btn disable'>
                <p>Commander</p>
            </div>}
        </form>
    )
}
