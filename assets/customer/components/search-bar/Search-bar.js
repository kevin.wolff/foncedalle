import React, { useState, useContext } from 'react'
import ShoppingContext from '../../ShoppingContext'
import { MdSearch, MdExpandLess, MdExpandMore, MdChevronRight } from 'react-icons/md'
import { BiFilterAlt } from 'react-icons/bi'

export default function SearchBar()  {

    // Valeur du champ de recherche
    const [ inputValue, setInputValue ] = useState('')

    // Commentaire dans shoppingContext.js
    const {
        familiesData,
        fetchWithFilter,
    } = useContext(ShoppingContext)

    // Bouton pour afficher/masquer bar de recherche en display mobile
    const searchBar = document.querySelector('#search-bar')
    const displayMobileSearchBar = () => {
        if (searchBar.classList.contains('toggle-mobile')) {
            searchBar.classList.remove('toggle-mobile')
        } else {
            searchBar.classList.add('toggle-mobile')
        }
    }

    return (
        <div id='search-bar'>
            <div onClick={ displayMobileSearchBar } className="search-bar-mobile-menu">
                <BiFilterAlt />
            </div>
            <div className='control search'>
                <label htmlFor='searchProduct'>
                    <MdSearch id='searchProduct' onClick={() => { fetchWithFilter('query', inputValue) }}/>
                </label>
                <input value={ inputValue } onChange={ event => setInputValue( event.target.value ) } type='text' name='searchProduct' pattern='[a-z0-9A-Zéèàê ]*' />
            </div>
            <div className='control price'>
                <p>Prix</p>
                <MdExpandMore onClick={() => { fetchWithFilter('order', `DESC`) }}/>
                <MdExpandLess onClick={() => { fetchWithFilter('order', `ASC`) }}/>
            </div>
            <div className='control price mobile'>
                <p onClick={() => { fetchWithFilter('order', `DESC`) }}>Prix croissant</p>
                <p onClick={() => { fetchWithFilter('order', `ASC`) }}>Prix décroissant</p>
            </div>
            <div className='control discount'>
                <p>Populaires</p>
                <p>Réductions</p>
            </div>
            <div className='control family mobile'>
                <ul>
                    <li onClick={() => { fetchWithFilter('family') }}>Tous les produits <MdChevronRight/></li>
                    { familiesData.data.map(family => (
                        <li onClick={() => { fetchWithFilter('family', family.name) }} key={ family.id }>{ family.name } <MdChevronRight/></li>
                    ))}
                </ul>
            </div>
        </div>
    )
}
