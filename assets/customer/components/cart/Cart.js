import React, { useState, useContext }  from 'react'
import ShoppingContext from '../../ShoppingContext'
import ProductsList from '../products-list/Products-list'
import OrderForm from '../order-form/Order-form'
import { MdChevronLeft, MdErrorOutline, MdFavoriteBorder } from 'react-icons/md'
import { SiInstagram, SiSnapchat, SiTiktok } from 'react-icons/si'

export default function Cart() {

    // Gere l'affichage du composant selon la réponse HTTP du post, 0: pas de post, 1: erreur et autres, 201: success
    const [ requestStatus, setRequestStatus ] = useState({ status: 0, statusText: '' })

    // Commentaire dans shoppingContext.js
    const {
        productsInCart,
        clearProductFromCart,
        cartCompView,
        updateCartCompView,
        orderFormCompView,
        updateOrderFormCompView
    } = useContext(ShoppingContext)

    // Update l'affichage du composant selon la réponse HTTP du post
    const requestResponse = (status, statusText) => {
        setRequestStatus({ status: status, statusText: statusText })
    }

    // Si le formulaire soumis et réponse HTTP
    if (requestStatus.status > 0) {
        return (
            <div id='cart'>
                <div onClick={() => { updateCartCompView(cartCompView === false ? true : false) } } className='cart-quit-btn'>
                    <MdChevronLeft />
                    <p>Revenir à la boutique</p>
                </div>
                <div className='wrapper'>
                    { requestStatus.status === 201 &&
                    <div className='order-succes'>
                        <p className='status-msg'>Votre commande est enregistré ! <MdFavoriteBorder /></p>
                        <p>Nous vous remercions pour votre confiance</p>
                        <p>Suivez notre actualité</p>
                        <div className='wrapper-network'>
                            <a href=''><SiInstagram /></a>
                            <a href=''><SiSnapchat /></a>
                            <a href=''><SiTiktok /></a>
                        </div>
                        <a href=''>Nous contacter</a>
                    </div>}
                    { requestStatus.status === 1 &&
                    <div className='order-fail'>
                        <p className='status-msg'>Votre commande à rencontré un problème <MdErrorOutline /></p>
                        <p>Nous sommes désolé pour ce désagrément</p>
                        <p>N'hésitez pas à nous <a href="">contacter</a></p>
                    </div>}
                </div>
            </div>
        )
    }

    return (
        <div id='cart'>
            <div onClick={() => { updateCartCompView(cartCompView === false ? true : false) } } className='cart-quit-btn'>
                <MdChevronLeft />
                <p>Revenir à la boutique</p>
            </div>
            <div className='wrapper'>
                <ProductsList />
                { productsInCart.length > 0 && orderFormCompView === false &&
                <div className='wrapper-btn'>
                    <div onClick={() => { clearProductFromCart([]) }} className='btn-clear-cart'>
                        <p>Effacer le panier</p>
                    </div>
                    <div onClick={() => { updateOrderFormCompView(true) }} className='btn continu-order'>
                        <p>Valider commande</p>
                    </div>
                </div>}
                { productsInCart.length > 0 && orderFormCompView === true &&
                <OrderForm requestResponse={ requestResponse } />}
            </div>
        </div>
    )
}
