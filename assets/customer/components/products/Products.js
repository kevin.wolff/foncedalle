import React, { useContext } from 'react'
import ShoppingContext from '../../ShoppingContext'
import { RiShoppingBag2Line } from 'react-icons/ri'

export default function Products() {

    // Commentaire dans shoppingContext.js
    const {
        productsData,
        query,
        family,
        currentPage,
        productsPerPage,
        addProductInCart
    } = useContext(ShoppingContext)

    // Découpe le tableau des produits en page de 15 produits
    const indexOfLastPage = currentPage * productsPerPage
    const indexOfFirtPage = indexOfLastPage - productsPerPage
    const currentProducts = productsData.data.slice(indexOfFirtPage, indexOfLastPage)

    // Si le fetch est en loading ou erreur, ou si le tableau de produit est vide
    if (productsData.loading || productsData.error || currentProducts.length < 1) {
        let textToDisplay = ''
        switch (true) {
            case productsData.loading:
                textToDisplay = 'Chargement'
                break
            case productsData.error:
                textToDisplay = 'Une erreur est survenue'
                break
            case currentProducts.length < 1:
                textToDisplay = 'Aucun résultats'
        }
        return (
            <div id="products">
                { family && !query &&
                <p className='products-search'>Résultat(s) pour <span>{ family }</span></p>}
                { !family && query &&
                <p className='products-search'>Résultat(s) pour <span>{ query }</span></p>}
                { family && query &&
                <p className='products-search'>Résultat(s) pour <span>{ query }</span> dans <span>{ family }</span></p>}
                <div className='loading-animation products'>
                    <p>{ textToDisplay }</p>
                    <div className='wrapper-loading-dots'>
                        <span className='loading-dot'/>
                        <span className='loading-dot'/>
                        <span className='loading-dot'/>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div id='products'>
            { family && !query &&
            <p className='products-search'>Résultat(s) pour <span>{ family }</span></p>}
            { !family && query &&
            <p className='products-search'>Résultat(s) pour <span>{ query }</span></p>}
            { family && query &&
            <p className='products-search'>Résultat(s) pour <span>{ query }</span> dans <span>{ family }</span></p>}
            <div className='products-container'>
                { currentProducts.map(product => (
                    <div key={ product.id } className='product-wrapper'>
                        <div className='product-img'>
                            <img src={ `/images/products/${product.imageFile}` } alt=''/>
                        </div>
                        <div className='product-name'>
                            <p>{ product.name }</p>
                        </div>
                        <div className='product-description'>
                            <p>{ product.description }</p>
                        </div>
                        <div className='product-price'>
                            <p>{ product.price }€</p>
                        </div>
                        <div onClick={() => addProductInCart(product) } className='btn'>
                            <p>Ajouter </p>
                            <RiShoppingBag2Line/>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}
