import React, { useContext } from 'react'
import ShoppingContext from '../../ShoppingContext'
import { MdChevronRight } from 'react-icons/md'
import { RiShoppingBag2Line } from 'react-icons/ri'

export default function SideMenu() {

    // Commentaire dans shoppingContext.js
    const {
        familiesData,
        fetchWithFilter,
        cartInfos,
        cartCompView,
        updateCartCompView
    } = useContext(ShoppingContext)

    if (familiesData.loading || familiesData.error) {
        return (
            <div>
            </div>
        )
    }

    return (
        <div id='side-menu'>
            <div className='side-cart'>
                <div onClick={() => { updateCartCompView(cartCompView === false ? true : false)}} className='btn' className='cart-display'>
                    <RiShoppingBag2Line/>
                    <div className='cart-qty'>
                        <p>{ cartInfos.productsQty }</p>
                    </div>
                </div>
                <div onClick={() => { updateCartCompView(cartCompView === false ? true : false)}} className='btn'>
                    <p>Voir le panier</p>
                </div>
            </div>
            <div className='side-navbar'>
                <ul>
                    <li onClick={() => { fetchWithFilter('family') }}>Tous les produits <MdChevronRight/></li>
                    { familiesData.data.map(family => (
                        <li onClick={() => { fetchWithFilter('family', family.name) }} key={ family.id }>{ family.name } <MdChevronRight/></li>
                    ))}
                </ul>
            </div>
        </div>
    )
}
