import React, { useContext }  from 'react'
import ShoppingContext from '../../ShoppingContext'
import { FaMinus, FaPlus } from 'react-icons/fa'

export default function ProductsList() {

    // Commentaire dans shoppingContext.js
    const {
        productsInCart,
        cartInfos,
        addProductInCart,
        removeProductFromCart
    } = useContext(ShoppingContext)

    return (
        <div id='products-list'>
            <h1>Votre panier</h1>
            <div className='cart-products list'>
                {productsInCart.length === 0 &&
                <p>Votre panier est vide</p>
                }
                <table>
                    <tbody>
                    { productsInCart.map(product => (
                        <tr key={ product.id }>
                            <td onClick={() => { removeProductFromCart(product) }}><FaMinus /></td>
                            <td onClick={() => { addProductInCart(product) }}><FaPlus /></td>
                            <td className='product-name'>{ product.name }</td>
                            <td className='product-qty'>{ product.qty }</td>
                            <td className='product-price'>{ product.price } €</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
            <div className='cart-products total'>
                <table>
                    <tbody>
                    <tr>
                        <td className='total-title'>Total</td>
                        <td className='total-products-qty'>{ cartInfos.productsQty }</td>
                        <td className='total-products-price'>{ cartInfos.totalTTC } €</td>
                    </tr>
                    <tr className='total-tva'>
                        <td className='total-title'>Inclus TVA</td>
                        <td className='total-products-qty'>20%</td>
                        <td className='total-products-price'>{ cartInfos.totalTax } €</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}
