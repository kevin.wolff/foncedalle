import React, { useContext } from 'react'
import ShoppingContext from '../../ShoppingContext'

export default function Pagination( ) {

    // Commentaire dans shoppingContext.js
    const {
        productsData,
        productsPerPage,
        currentPage,
        changePage
    } = useContext(ShoppingContext)

    // Nombre total de produits divisé par produits par page (15) = tableau du nombre de page
    const pageNumbers = []
    for(let i = 1; i <= Math.ceil(productsData.data.length / productsPerPage); i++) {
        pageNumbers.push(i)
    }

    // Fctn pour attribuer le style "active" au bouton actif
    const pageBtn = document.querySelectorAll('#pagination > div')
    pageBtn.forEach((element) => {
        if (element.id === `${ currentPage }`) {
            element.classList.add('active')
        }
        else {
            element.classList.remove('active')
        }
    })

    return (
        <div id='pagination'>
            { pageNumbers.map(number => (
                <div id={ number } key={ number } onClick={() => { changePage(number) }} className='btn'>
                    <p>{ number }</p>
                </div>
            ))}
        </div>
    )
}
