function slider() {

    const controllers = document.querySelectorAll('.slider-btn')
    const slides = document.querySelectorAll('.slider-img')
    const dots = document.querySelectorAll('.slider-dots')
    let x = 0
    let setX = 0

    dotsMove()
    let slideAuto = setTimeout(slideMoveAuto, 5000)
    controllers.forEach( (element) => {
        element.addEventListener('click', () => {
            slideMoveManual(element)
        })
    })

    function slideMoveAuto() {
        if (setX === -100 * (slides.length -1)) {
            setX = 0
            x = setX
        }
        else {
            x = setX -100
            setX = x
        }
        slides.forEach((element) => {
            element.style.transform = `translateX(${x}%)`
        })
        dotsMove()
        slideAuto = setTimeout(slideMoveAuto, 5000)
    }

    const slideMoveManual = (element) => {
        if (element.classList.contains('left')) {
            if ( setX === 0 ) {
                setX = -100 * (slides.length -1)
                x = setX
            }
            else {
                x = setX +100
                setX = x
            }
        }
        else if (element.classList.contains('right')) {
            if (setX === -100 * (slides.length -1)) {
                setX = 0
                x = setX
            }
            else {
                x = setX -100
                setX = x
            }
        }
        slides.forEach((element) => {
            element.style.transform = `translateX(${x}%)`
        })
        dotsMove()
        clearInterval(slideAuto)
        slideAuto = setTimeout(slideMoveAuto, 5000)
    }

    function dotsMove() {
        dots.forEach((element) => {
            element.classList.remove('active')
        })
        dots[Math.abs(x/100)].classList.add('active')
    }
}

export { slider }
