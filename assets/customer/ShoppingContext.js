import React, { createContext } from 'react'

export default createContext({

    // Hook get - valeur de l'url utilisée pour le fetch produit
    urlProducts: '',
    // Hook set - update de l'url utilisée pour le fetch produit
    updateUrlProducts: (newUrl) => {},

    // Hook get - tableau des familles
    familiesData: [],
    // Hook get - tableau de produits
    productsData: [],

    // Hook get - valeur du trie par famille en cours
    family: '',
    // Hook get - valeur de la dernière query produit
    query: '',
    // Fctn pour modifier l'url fetch avec une recherche par nom, trie par prix, etc
    fetchWithFilter: (type, request) => {},

    // Hook get - tableau de produits dans panier d'achat
    productsInCart: [],
    // Hook get - information relative au panier d'achat: quantité, total TTC, total HT, total taxes
    cartInfos: {},
    // Fctn pour ajouter un produit au panier d'achat
    addProductInCart: (product) => {},
    // Fctn pour enlever un produit au panier d'achat
    removeProductFromCart: (product) => {},
    // Hook set - update du panier d'achat
    clearProductFromCart: ([]) => {},

    // Hook set - valeur définie de produit par page
    productsPerPage: '',
    // Hook get - valeur de la page de produits actuelle
    currentPage: '',
    // Hook set - update de la page de produits
    changePage: (pageNumber) => {},

    // Hook get - boolean affichage du composant panier d'achat
    cartCompView: Boolean,
    // Hook set - update l'affichage du composant panier d'achat
    updateCartCompView: () => {},
    // Hook get - boolean affichage du composant formulaire de commande
    orderFormCompView: Boolean,
    // Hook set - update l'affichage du composant formulaire de commande
    updateOrderFormCompView: () => {}
})
