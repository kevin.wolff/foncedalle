import React from 'react'

export default function HandleResize() {
    const header = document.querySelector('#header')
    const searchBar = document.querySelector('#search-bar')
    let productsPerPage = Number

    if (window.innerWidth < 397) {
        productsPerPage = 5
    } else if (window.innerWidth >= 397 && window.innerWidth < 588) {
        productsPerPage = 8
    } else if (window.innerWidth >= 588 && window.innerWidth < 768) {
        productsPerPage = 12
    } else if (window.innerWidth >= 768 && window.innerWidth < 951) {
        // Breakpoint entre mobile et tablette, masque les menus mobiles
        header.classList.remove('toggle-mobile')
        searchBar.classList.remove('toggle-mobile')
        productsPerPage = 12
    } else if (window.innerWidth >= 951 && window.innerWidth < 1215) {
        productsPerPage = 16
    } else {
        productsPerPage = 20
    }
    return productsPerPage
}
