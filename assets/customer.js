import React from 'react'
import ReactDOM from 'react-dom'
import './customer/layout.scss'

import { header } from './customer/components/header/header'
import { slider } from './customer/components/slider/slider'

import Layout from './customer/Layout'

// Fctn resize pour responsivité du header
header()
// Fctn pour le slider
slider()

ReactDOM.render(
        <div id='layout'>
            <Layout/>
        </div>,
    document.getElementById('root')
)
