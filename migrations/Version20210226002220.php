<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210226002220 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `admin` (id INT AUTO_INCREMENT NOT NULL, user_name VARCHAR(20) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `campagne` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, image_file VARCHAR(255) DEFAULT NULL, available TINYINT(1) NOT NULL, updated DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `customer` (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, name VARCHAR(25) NOT NULL, surname VARCHAR(25) NOT NULL, email VARCHAR(100) NOT NULL, phone_number VARCHAR(10) NOT NULL, address VARCHAR(100) DEFAULT NULL, zip_code VARCHAR(10) DEFAULT NULL, rgpd_agree TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_81398E098D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `family` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, updated DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, ammount VARCHAR(10) NOT NULL, get_method VARCHAR(9) NOT NULL, get_date VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, status VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order_details` (id INT AUTO_INCREMENT NOT NULL, order_id_id INT DEFAULT NULL, product_id INT DEFAULT NULL, quantity VARCHAR(10) NOT NULL, price VARCHAR(10) NOT NULL, INDEX IDX_845CA2C1FCDAEAAA (order_id_id), INDEX IDX_845CA2C14584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `product` (id INT AUTO_INCREMENT NOT NULL, family_id INT DEFAULT NULL, name VARCHAR(25) NOT NULL, description VARCHAR(25) NOT NULL, price VARCHAR(5) NOT NULL, image_file VARCHAR(255) DEFAULT NULL, available TINYINT(1) NOT NULL, updated DATETIME NOT NULL, INDEX IDX_D34A04ADC35E566A (family_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `customer` ADD CONSTRAINT FK_81398E098D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE `order_details` ADD CONSTRAINT FK_845CA2C1FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE `order_details` ADD CONSTRAINT FK_845CA2C14584665A FOREIGN KEY (product_id) REFERENCES `product` (id)');
        $this->addSql('ALTER TABLE `product` ADD CONSTRAINT FK_D34A04ADC35E566A FOREIGN KEY (family_id) REFERENCES `family` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `product` DROP FOREIGN KEY FK_D34A04ADC35E566A');
        $this->addSql('ALTER TABLE `customer` DROP FOREIGN KEY FK_81398E098D9F6D38');
        $this->addSql('ALTER TABLE `order_details` DROP FOREIGN KEY FK_845CA2C1FCDAEAAA');
        $this->addSql('ALTER TABLE `order_details` DROP FOREIGN KEY FK_845CA2C14584665A');
        $this->addSql('DROP TABLE `admin`');
        $this->addSql('DROP TABLE `campagne`');
        $this->addSql('DROP TABLE `customer`');
        $this->addSql('DROP TABLE `family`');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE `order_details`');
        $this->addSql('DROP TABLE `product`');
    }
}
