
USE foncedalle;
/*CREATE FAMILY DATA*/
INSERT INTO family VALUES ('1', 'Gateaux apéritifs', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('2', 'Pizzas surgelées', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('3', 'Biscuits et gateaux', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('4', 'Bonbons', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('5', 'Chocolats', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('6', 'Canettes', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('7', 'Bouteilles', '2020-12-01 00:00:00');
INSERT INTO family VALUES ('8', 'Accessoires', '2020-12-01 00:00:00');

/*CREATE PRODUCT DATA*/
INSERT INTO product VALUES ('1', '1', 'Curly', 'Sachet de 130g', '2.20', 'curly.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('2', '1', 'Doritos', 'Sachet de 130g', '2.80', 'doritos.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('3', '1', 'Doritos', 'Sachet de 130g', '3.00', 'doritos-nature.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('4', '1', 'Doritos', 'Sachet de 130g', '2.50', 'doritos-cream.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('5', '1', 'Pringles', 'Sachet de 130g', '3.00', 'pringles.jpeg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('6', '2', 'Jambon fromage', 'poid total 150G', '5.00', 'ristorante-jambon.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('7', '2', 'Mozarella', 'poid total 150G', '5.00', 'ristorante-mozarella.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('8', '2', 'Quatre fromage', 'poid total 150G', '5.00', 'ristorante-quatre-fromage.jpg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('9', '3', 'Dinosaurus', 'Sachet de 150g', '2.80', 'dinosaurus-lotus.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('10', '3', 'Pims', 'Sachet de 150g', '2.80', 'pims-lu.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('11', '3', 'Prince', 'Sachet de 150g', '2.80', 'prince-lu.jpeg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('12', '4', 'Mini-mix Carambar', 'Sachet de 100g', '2.80', 'mini-mix-carambar.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('13', '4', 'Pailles pik Haribo', 'Sachet de 100g', '2.80', 'pailles-pik-haribo.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('14', '4', 'Skittles', 'Sachet de 100g', '3.00', 'skittles.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('15', '4', 'Dragibus', 'Sachet de 100g', '3.00', 'dragibus.jpg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('16', '5', 'Kinder délice', 'poid total 150G', '4.00', 'delice-kinder.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('17', '5', 'KitKat', 'poid total 150G', '3.80', 'kitkat-nestle.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('18', '5', 'Kinder shokobon', 'poid total 150G', '3.80', 'shoko-bon-kinder.jpg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('19', '6', 'Coca Cola', '0,33cl', '2.10', 'coca-cola-0l33.png', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('20', '6', 'Fanta', '0,33cl', '2.10', 'fanta-0l33.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('21', '6', 'Monster', '0,33cl', '2.10', 'monster-0l5.jpg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('22', '7', 'Coca Cola', '1l5', '3.80', 'coca-cola-1l5.jpg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('23', '7', 'Coca Cola zéro', '1l5', '3.80', 'coca-zero-1l5.jpeg', '1', '2020-12-01 00:00:00');
INSERT INTO product VALUES ('24', '7', 'Fanta', '1l5', '3.80', 'fanta-orange-1l5.jpg', '1', '2020-12-01 00:00:00');

INSERT INTO product VALUES ('25', '8', 'Briquet', 'Bic', '3.00', 'briquet.jpg', '1', '2020-12-01 00:00:00');

/*CREATE CAMPAGNE DATA*/
INSERT INTO campagne VALUES ('1', 'Campagne 1', 'campagne-1.jpg', '1', '2020-12-01-00:00:00');
INSERT INTO campagne VALUES ('2', 'Campagne 2', 'campagne-2.jpg', '1', '2020-12-01-00:00:00');
INSERT INTO campagne VALUES ('3', 'Campagne 3', 'campagne-3.jpg', '1', '2020-12-01-00:00:00');

/*CREATE ORDER DATA*/
INSERT INTO order VALUES('1', '17.50', 'livraison', '31-03-2021', '31-03-2021 00:00:00', '2');
INSERT INTO order VALUES('2', '9.90', 'boutique', '02-04-2021', '01-04-2021 00:00:00', '0');
INSERT INTO order VALUES('3', '12.10', 'livraison', '03-04-2021', '03-04-2021 00:00:00', '0');
INSERT INTO order VALUES('4', '9.20', 'boutique', '03-04-2021', '02-04-2021 00:00:00', '0');

/*CREATE CUSTOMER DATA*/
INSERT INTO customer VALUES ('1', '1', 'Wolff', 'Kévin', 'kevin.wolff@live.fr', '0700256987', '130 Cours Berriat, app 10', '38000', '1');
INSERT INTO customer VALUES ('2', '2', 'Michalon', 'Liswag', 'liswag@gmail.com', '0698526987', '21 Rue du soleil, app 3', '38000', '1');
INSERT INTO customer VALUES ('3', '3', 'Thullier', 'Etienne', 'frelon@outlook.com', '0430859674', '9b Boulevard Albert 1er', '38100', '0');
INSERT INTO customer VALUES ('4', '4', 'Zitouni', 'Walid', 'patisserie@artisant.fr', '0600256987', '60 Rue Ampère', '38000', '1');

/*CREATE ORDER DETAILS DATA*/
INSERT INTO order_details VALUES('1', '1' '6', '2', '10.00');
INSERT INTO order_details VALUES('2', '1' '7', '1', '5.00');
INSERT INTO order_details VALUES('3', '1' '4', '1', '2.50');
INSERT INTO order_details VALUES('4', '2' '19', '1', '2.10');
INSERT INTO order_details VALUES('5', '2' '9', '1', '2.80');
INSERT INTO order_details VALUES('6', '2' '4', '2', '5.00');
INSERT INTO order_details VALUES('7', '3' '16', '1', '4.00');
INSERT INTO order_details VALUES('8', '3' '25', '1', '3.00');
INSERT INTO order_details VALUES('9', '3' '15', '1', '3.00');
INSERT INTO order_details VALUES('10', '3' '24', '1', '2.10');
INSERT INTO order_details VALUES('11', '4' '21', '2', '4.20');
INSERT INTO order_details VALUES('12', '4' '8', '1', '5.00');