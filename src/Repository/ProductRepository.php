<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findAvailableProducts($order, $query, $family)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.available = 1');

        if ($order) {
            $qb->orderBy('p.price', $order);
        }

        if ($query) {
            $qb->setParameter('query', $query)
                ->andWhere('p.name = :query');
        }

        if ($family) {
            $qb->join('p.family', 'f')
                ->setParameter('family', $family)
                ->andWhere('f.name = :family');
        }

        return $qb->getQuery()->getResult();
    }
}
