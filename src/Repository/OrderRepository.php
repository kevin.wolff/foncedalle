<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findAllInProgressOrder()
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.status = 0')
            ->orderBy('o.id', 'desc');
        return $qb->getQuery()->getResult();
    }

    public function findAllInWaitingOrder()
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.status = 2')
            ->orderBy('o.id', 'desc');
        return $qb->getQuery()->getResult();
    }
}
