<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="`product`")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("product:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     * @Groups("product:read")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-z0-9A-Zéèàê ]*$/", match=true, message="Lettres et chiffres uniquement")
     * @Assert\Length(min=3, minMessage="Minimum 3 caractères")
     * @Assert\Length(max=25, maxMessage="Maximum 25 caractères")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25)
     * @Groups("product:read")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-z0-9A-Zéèàê ]*$/", match=true, message="Lettres et chiffres uniquement")
     * @Assert\Length(min=3, minMessage="Minimum 3 caractères")
     * @Assert\Length(max=25, maxMessage="Maximum 25 caractères")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=5)
     * @Groups("product:read")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^\d{1,2}.\d{2}?$/", match=true, message="Format prix : 2.50")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("product:read")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="boolean")
     */
    private $available;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("product:read")
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity=Family::class, inversedBy="products")
     * @ORM\JoinColumn
     * @Groups("product:read")
     * @Assert\NotNull(message="Une famille doit être définie")
     */
    private $family;

    public function __construct()
    {
        $this->updated = new \DateTime();
    }

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEuroPrice(): ?string
    {
        return $this->getPrice(). '€';
    }

    public function getImageFile(): ?string
    {
        return $this->imageFile;
    }

    public function setImageFile(?string $imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile)
        {
            $this->updated = new \DateTime();
        }
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getFamily(): ?Family
    {
        return $this->family;
    }

    public function setFamily(?Family $family): self
    {
        $this->family = $family;

        return $this;
    }
}
