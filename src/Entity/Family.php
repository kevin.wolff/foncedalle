<?php

namespace App\Entity;

use App\Repository\FamilyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=FamilyRepository::class)
 * @ORM\Table(name="`family`")
 */
class Family
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"product:read", "family:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     * @Groups({"product:read", "family:read"})
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-z0-9A-Zéèàê ]*$/", match=true, message="Lettres et chiffres uniquement")
     * @Assert\Length(min=3, minMessage="Minimum 3 caractères")
     * @Assert\Length(max=25, maxMessage="Maximum 25 caractères")
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"product:read", "family:read"})
     * @ORM\GeneratedValue
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="family")
     */
    private $products;

    public function __construct()
    {
        $this->updated = new \DateTime();
        $this->products = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setFamily($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getFamily() === $this) {
                $product->setFamily(null);
            }
        }

        return $this;
    }
}
