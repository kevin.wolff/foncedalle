<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Customer::class, mappedBy="order", cascade={"persist", "remove"})
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=OrderDetails::class, mappedBy="orderId", cascade={"persist", "remove"})
     */
    private $orderDetails;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^\d{1,2}.\d{2}?$/", match=true, message="Format prix : 2.50")
     */
    private $ammount;

    /**
     * @ORM\Column(type="string", length=9)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/[a-zA-Zéèàê ]*$/", match=true, message="Lettres uniquement, \'boutique\' ou \'livraison\'")
     * @Assert\Length(min=8, minMessage="Minimum 8 caractères")
     * @Assert\Length(max=9, maxMessage="Maximum 9 caractères")
     */
    private $getMethod;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[0-9]{2}\-[0-9]{2}\-[0-9]{4}$/", match=true, message="Exemple : 01-01-2021'")
     * @Assert\Length(min=10)
     * @Assert\Length(max=15)
     */
    private $getDate;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $status;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->orderDetails = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->idToString();
    }

    // Obligé de faire cette manipulation pour Doctrine
    public function idToString(): ?string
    {
        $id = $this->getId();
        return ''.$id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;
        $newOrder = null === $customer ? null : $this;
        if ($customer->getOrder() !== $newOrder) {
            $customer->setOrder($newOrder);
        }
        return $this;
    }

    /**
     * @return Collection|OrderDetails[]
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): self
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails[] = $orderDetail;
            $orderDetail->setOrderId($this);
        }
        return $this;
    }

    public function removeOrderDetail(OrderDetails $orderDetail): self
    {
        if ($this->orderDetails->removeElement($orderDetail)) {
            if ($orderDetail->getOrderId() === $this) {
                $orderDetail->setOrderId(null);
            }
        }
        return $this;
    }

    public function getAmmount(): ?string
    {
        return $this->ammount;
    }

    public function setAmmount(string $ammount): self
    {
        $this->ammount = $ammount;
        return $this;
    }

    public function getEuroAmmount(): ?string
    {
        return $this->getAmmount(). '€';
    }

    public function getGetMethod(): ?string
    {
        return $this->getMethod;
    }

    public function setGetMethod(string $getMethod): self
    {
        $this->getMethod = $getMethod;
        return $this;
    }

    public function getGetDate(): ?string
    {
        return $this->getDate;
    }

    public function setGetDate(string $getDate): self
    {
        $this->getDate = $getDate;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
