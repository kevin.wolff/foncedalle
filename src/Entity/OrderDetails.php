<?php

namespace App\Entity;

use App\Repository\OrderDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrderDetailsRepository::class)
 * @ORM\Table(name="`order_details`")
 */
class OrderDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderDetails")
     */
    private $orderId;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[0-9]$/", match=true, message="Chiffre ou nombre")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^\d{1,2}.\d{2}?$/", match=true, message="Format prix : 2.50")
     */
    private $price;

    public function __toString(): ?string
    {
        return $this->getProduct();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderId(): ?Order
    {
        return $this->orderId;
    }

    public function setOrderId(?Order $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getEuroPrice(): ?string
    {
        return $this->getPrice(). '€';
    }
}
