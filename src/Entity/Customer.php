<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 * @ORM\Table(name="`customer`")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-zA-Zéèàê\- ]*$/", match=true, message="Lettres uniquement, minimum 2 maximum 25")
     * @Assert\Length(min=2, minMessage="Minimum 2 caractères")
     * @Assert\Length(max=25, maxMessage="Maximum 25 caractères")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-zA-Zéèàê\- ]*$/", match=true, message="Lettres uniquement, minimum 2 maximum 25")
     * @Assert\Length(min=2, minMessage="Minimum 2 caractères")
     * @Assert\Length(max=25, maxMessage="Maximum 25 caractères")
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/", match=true, message="Exemple: foncedalle@gmail.com")
     * @Assert\Length(min=3, minMessage="Minimum 3 caractères")
     * @Assert\Length(max=100, maxMessage="Maximum 100 caractères")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[0-9]{1,10}$/", match=true, message="Exemple: 0611007474")
     * @Assert\Length(min=10, minMessage="Minimum 10 caractères")
     * @Assert\Length(max=10, maxMessage="Maximum 10 caractères")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Regex(pattern="/^[a-z0-9A-Zéèàê\-\, ]*$/", match=true, message="Lettres et chiffres uniquement")
     * @Assert\Length(min=2, minMessage="Minimum 2 caractères")
     * @Assert\Length(max=100, maxMessage="Maximum 100 caractères")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Regex(pattern="/^[0-9]{1,10}$/", match=true, message="Exemple: 38000")
     * @Assert\Length(min=2, minMessage="Minimum 2 caractères")
     * @Assert\Length(max=5, maxMessage="Maximum 5 caractères")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $rgpdAgree;

    /**
     * @ORM\OneToOne(targetEntity=Order::class, inversedBy="customer", cascade={"persist", "remove"})
     */
    private $order;

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;
        return $this;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function setZipCode($zipCode): self
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getRgpdAgree(): ?bool
    {
        return $this->rgpdAgree;
    }

    public function setRgpdAgree(bool $rgpdAgree): self
    {
        $this->rgpdAgree = $rgpdAgree;
        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;
        return $this;
    }
}
