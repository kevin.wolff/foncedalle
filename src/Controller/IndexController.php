<?php

namespace App\Controller;

use App\Entity\Campagne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $sliders = $this->getDoctrine()
            ->getRepository(Campagne::class)
            ->findBy(array('available' => '1'));

        return $this->render('index.html.twig', [
            'sliders' => $sliders
        ]);
    }
}
