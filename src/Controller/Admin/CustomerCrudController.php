<?php

namespace App\Controller\Admin;

use App\Entity\Customer;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CustomerCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Customer::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-pen')
                    ->setLabel('');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel('');
            })

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action
                    ->setLabel('Editer et continuer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setLabel('Editer');
            })

            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Profil client')
            ->setEntityLabelInPlural('Profil clients')
            ->setPageTitle('new', 'Nouveau %entity_label_singular%')
            ->setPageTitle('edit', 'Edition %entity_label_singular%')
            ->setPageTitle('detail', 'Détail %entity_label_singular%')
            ->setSearchFields(['id', 'name', 'surname', 'email', 'phoneNumber', 'address', 'zipCode'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            TextField::new('name', 'Nom')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Nom de famille',
                        'pattern' => '[a-zA-Zéèàê\- ]*$',
                        'maxLength' => '25',
                        'minLength'=> '2',
                        'title' => 'Lettres uniquement, minimum 2 maximum 25']),
            TextField::new('surname', 'Prénom')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Prénom',
                        'pattern' => '[a-zA-Zéèàê\- ]*$',
                        'maxLength' => '25',
                        'minLength'=> '2',
                        'title' => 'Lettres uniquement, minimum 2 maximum 25']),
            TextField::new('email', 'Email')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Adresse email',
                        'pattern' => '[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+',
                        'maxLength' => '100',
                        'minLength'=> '3',
                        'title' => 'Exemple: foncedalle@gmail.com']),
            TextField::new('phoneNumber', 'Numéro')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Numéro de téléphone, ex : 0611007474',
                        'pattern' => '^[0-9]{1,10}$',
                        'maxLength' => '10',
                        'minLength'=> '10',
                        'title' => 'Exemple: 0611007474']),
            TextField::new('address', 'Adresse')
                ->setFormTypeOption('attr',
                    ['placeholder' => '130 Cours Berriat, app 8',
                        'pattern' => '^[a-z0-9A-Zéèàê\-\, ]$',
                        'maxLength' => '100',
                        'minLength'=> '2',
                        'title' => 'Lettres, chiffres et caractères spéciaux']),
            TextField::new('zipCode', 'Code postal')
                ->setFormTypeOption('attr',
                    ['placeholder' => '38000',
                        'pattern' => '^[0-9]{1,10}$',
                        'maxLength' => '5',
                        'minLength'=> '2',
                        'title' => 'Exemple: 38000']),
            BooleanField::new('rgpdAgree', 'RGPD')
                ->setFormTypeOption('attr',
                    ['checked' => true]),
            AssociationField::new('order', 'N° commande'),
        ];
    }
}
