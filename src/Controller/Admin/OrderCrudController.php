<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-pen')
                    ->setLabel('');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel('');
            })

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action
                    ->setLabel('Editer et continuer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setLabel('Editer');
            })

            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Commande')
            ->setEntityLabelInPlural('Commandes')
            ->setPageTitle('new', 'Nouvelle %entity_label_singular%')
            ->setPageTitle('edit', 'Edition %entity_label_singular%')
            ->setPageTitle('detail', 'Détail %entity_label_singular%')
            ->setSearchFields(['id', 'ammount', 'getMethod', 'getDate', 'createdAt'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            AssociationField::new('customer', 'Client'),
            ArrayField::new('orderDetails', 'Détails commande')
                ->hideOnForm(),
            TextField::new('ammount', 'Total TTC')
                ->onlyOnForms()
                ->setFormTypeOption('attr',
                    ['placeholder' => 'exemple: 2.20',
                        'pattern' => '^\d{1,2}\.\d{2}?$',
                        'maxLength' => '10',
                        'minLength'=> '4',
                        'title' => 'Uniquement format 2.00 ou 11.00']),
            TextField::new('EuroAmmount', 'Total TTC')
                ->hideOnForm(),
            TextField::new('getMethod', 'Récupération')
                ->setFormTypeOption('attr',
                    ['placeholder' => '\'boutique\' ou \'livraison\'',
                        'pattern' => '[a-zA-Zéèàê ]*$',
                        'maxLength' => '9',
                        'minLength'=> '8',
                        'title' => 'Lettres uniquement, \'boutique\' ou \'livraison\'']),
            TextField::new('getDate', 'Pour le')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'exemple: 01-01-2021',
                        'pattern' => '^[0-9]{2}\-[0-9]{2}\-[0-9]{4}$',
                        'maxLength' => '15',
                        'minLength'=> '10',
                        'title' => 'Exemple : 01-01-2021']),
            DateTimeField::new('createdAt', 'Créée le')
                ->hideOnForm()
                ->setFormat('dd-MM-YYYY'),
            ChoiceField::new('status', 'Statut')
                ->setChoices([
                    'En cours' => '0',
                    'Validé' => '1',
                    'En attente' => '2']),
        ];
    }
}
