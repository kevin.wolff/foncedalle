<?php

namespace App\Controller\Admin;

use App\Entity\Family;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FamilyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Family::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-pen')
                    ->setLabel('');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel('');
            })

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action
                    ->setLabel('Editer et continuer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setLabel('Editer');
            })

            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Famille')
            ->setEntityLabelInPlural('Familles')
            ->setPageTitle('new', 'Nouvelle %entity_label_singular%')
            ->setPageTitle('edit', 'Edition %entity_label_singular%')
            ->setPageTitle('detail', 'Détail %entity_label_singular%')
            ->setSearchFields(['id', 'name'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            TextField::new('name', 'Nom')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Nom de la famille',
                    'pattern' => '[a-z0-9A-Zéèàê ]*',
                    'maxLength' => '25',
                    'minLength'=> '3',
                    'title' => 'Lettres et chiffres uniquement, minimum 3 maximum 25']),
            DateTimeField::new('updated', 'Mise à jour')
                ->hideOnForm()
                ->setFormat('dd-MM-YYYY'),
            ArrayField::new('products', 'Produits')
                ->hideOnForm(),
        ];
    }
}
