<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-pen')
                    ->setLabel('');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel('');
            })

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action
                    ->setLabel('Editer et continuer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setLabel('Editer');
            })

            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produit')
            ->setEntityLabelInPlural('Produits')
            ->setPageTitle('new', 'Nouveau %entity_label_singular%')
            ->setPageTitle('edit', 'Edition %entity_label_singular%')
            ->setPageTitle('detail', 'Détail %entity_label_singular%')
            ->setSearchFields(['id', 'name', 'price', 'family.name'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            AssociationField::new('family', 'Famille')
                ->setFormTypeOption('attr',
                    ['required' => 'required',
                    'title' => 'Selectionner une famille']),
            TextField::new('name', 'Nom')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Nom du produit',
                    'pattern' => '[a-z0-9A-Zéèàê ]*',
                    'maxLength' => '25',
                    'minLength'=> '3',
                    'title' => 'Lettres et chiffres uniquement, minimum 3 maximum 25']),
            TextField::new('description', 'Description')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Description du produit',
                    'pattern' => '[a-z0-9A-Zéèàê ]*',
                    'maxLength' => '25',
                    'minLength'=> '3',
                    'title' => 'Lettres et chiffres uniquement, minimum 3 maximum 25']),
            TextField::new('EuroPrice', 'Prix')
                ->hideOnForm(),
            TextField::new('price', 'Prix')
                ->onlyOnForms()
                ->setFormTypeOption('attr',
                    ['placeholder' => '2.00€',
                    'pattern' => '^\d{1,2}\.\d{2}?$',
                    'maxLength' => '5',
                    'minLength'=> '1',
                    'title' => 'Uniquement format 2.00 ou 11.00']),
            ImageField::new('imageFile', 'Image produit')
                ->setUploadDir('public/images/products')
                ->setBasePath('/images/products')
                ->setHelp('Taille maximum du fichier : 200ko'),
            BooleanField::new('available', 'Disponibilité'),
            DateTimeField::new('updated', 'Mise à jour')
                ->hideOnForm()
                ->setFormat('dd-MM-YYYY'),
        ];
    }
}
