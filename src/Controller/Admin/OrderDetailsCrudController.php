<?php

namespace App\Controller\Admin;

use App\Entity\OrderDetails;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderDetailsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderDetails::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action
                    ->setIcon('fa fa-pen')
                    ->setLabel('');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action
                    ->setIcon('fa fa-trash')
                    ->setLabel('');
            })

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action
                    ->setLabel('Editer et continuer');
            })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                return $action
                    ->setLabel('Editer');
            })

            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Panier')
            ->setEntityLabelInPlural('Paniers')
            ->setPageTitle('new', 'Nouveau %entity_label_singular%')
            ->setPageTitle('edit', 'Edition %entity_label_singular%')
            ->setPageTitle('detail', 'Détail %entity_label_singular%')
            ->setSearchFields(['id', 'quantity', 'price'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            AssociationField::new('orderId', 'N° commande'),
            AssociationField::new('product', 'Produit'),
            TextField::new('quantity', 'Quantité')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Quantité du produit',
                        'pattern' => '^[0-9]$',
                        'maxLength' => '10',
                        'minLength'=> '1',
                        'title' => 'Quantité du produit dans la commande']),
            TextField::new('EuroPrice', 'Prix total')
                ->hideOnForm(),
            TextField::new('price', 'Prix total')
                ->onlyOnForms()
                ->setFormTypeOption('attr',
                    ['placeholder' => '2.00€',
                        'pattern' => '^\d{1,2}\.\d{2}?$',
                        'maxLength' => '5',
                        'minLength'=> '1',
                        'title' => 'Uniquement format 2.00 ou 11.00']),
        ];
    }
}
