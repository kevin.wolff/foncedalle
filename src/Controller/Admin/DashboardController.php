<?php

namespace App\Controller\Admin;

use App\Entity\Family;
use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Entity\Product;
use App\Entity\Campagne;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $userName = $this->getUser()->getUsername();
        $currentDateTime = new \DateTime();
        $currentDateTime = $currentDateTime->format('d/m/Y');
        $progressOrders = $this->getDoctrine()->getRepository(Order::class)->findAllInProgressOrder();
        $nmbrProgressOrders = count($progressOrders);
        $waitingOrders = $this->getDoctrine()->getRepository(Order::class)->findAllInWaitingOrder();
        $nmbrWaitingOrders = count($waitingOrders);
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
            'date' => $currentDateTime,
            'userName' => $userName,
            'progressOrders' => $progressOrders,
            'nmbrProgressOrders' => $nmbrProgressOrders,
            'waitingOrders' => $waitingOrders,
            'nmbrWaitingOrders' => $nmbrWaitingOrders,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Foncedalle')
            ->setFaviconPath('favicon/favicon.ico');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Gestion');
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Admin', 'fa fa-user', Product::class)
            ->setController(AdminCrudController::class);
        yield MenuItem::linkToCrud('Campagnes', 'fa fa-bullhorn', Campagne::class)
            ->setController(CampagneCrudController::class);
        yield MenuItem::section('Inventaire');
        yield MenuItem::linkToCrud('Familles', 'fa fa-book', Family::class)
            ->setController(FamilyCrudController::class);
        yield MenuItem::linkToCrud('Produits', 'fa fa-archive', Product::class)
            ->setController(ProductCrudController::class);
        yield MenuItem::section('Shop');
        yield MenuItem::linkToCrud('Clients', 'fas fa-id-card', Customer::class)
            ->setController(CustomerCrudController::class);
        yield MenuItem::linkToCrud('Commandes', 'fas fa-shopping-cart', Order::class)
            ->setController(OrderCrudController::class);
        yield MenuItem::linkToCrud('Paniers', 'fas fa-info-circle', OrderDetails::class)
            ->setController(OrderDetailsCrudController::class);
    }
}
