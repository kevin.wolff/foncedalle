<?php

namespace App\Controller\Api;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiOrder extends AbstractController {

    /**
     * @Route("/api/order", name="api_post_order", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function newOrder(Request $request, EntityManagerInterface $entityManager)
    {
        $requestContent = json_decode($request->getContent(), true);

        // Nouvelle Order
        $order = new Order();
        $order->setAmmount($requestContent['ammount']);
        $order->setGetMethod($requestContent['getMethod']);
        $order->setGetDate($requestContent['getDate']);
        $order->setStatus($requestContent['status']);

        $entityManager->persist($order);
        $entityManager->flush();

        // Nouveau OrderDetail pour chaque articles dans le panier
        foreach ($requestContent['products'] as $product) {
            $orderDetails = new OrderDetails();
            $orderDetails->setOrderId($order);
            $orderDetails->setProduct($entityManager->find(Product::class, $product['id']));
            $orderDetails->setQuantity($product['qty']);
            $orderDetails->setPrice($product['qty'] * $product['price']);

            $entityManager->persist($orderDetails);
            $entityManager->flush();
        }

        // Nouveau Customer
        $customer = new Customer();
        $customer->setName($requestContent['name']);
        $customer->setSurname($requestContent['surname']);
        $customer->setEmail($requestContent['email']);
        $customer->setPhoneNumber($requestContent['phoneNumber']);
        $customer->setAddress($requestContent['address']);
        $customer->setZipCode($requestContent['zipCode']);
        $customer->setRgpdAgree($requestContent['rgpdAgree']);
        $customer->setOrder($order);

        $entityManager->persist($customer);
        $entityManager->flush();
        return $this->json(null, 201);
    }
}
