<?php

namespace App\Controller\Api;

use App\Repository\FamilyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiFamily extends AbstractController {

    /**
     * @Route("/api/families", name="api_get_families", methods={"GET"})
     * @param FamilyRepository $familyRepository
     * @return JsonResponse
     */
    public function findAll(FamilyRepository $familyRepository)
    {
        return $this->json($familyRepository->findAll(), 200, [], ['groups' => 'family:read']);
    }
}
