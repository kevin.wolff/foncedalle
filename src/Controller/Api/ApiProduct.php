<?php

namespace App\Controller\Api;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/products", name="api_products", methods={"GET"})
 */
class ApiProduct extends AbstractController
{
    /**
     * @Route(name="api_products_get_available")
     * @param ProductRepository $productRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function findAvailableProducts(ProductRepository $productRepository, Request $request)
    {
        $order = $request->query->get('order');
        $query = $request->query->get('query');
        $family = $request->query->get('family');

        return $this->json($productRepository->findAvailableProducts($order, $query, $family), 200, [], ['groups' => 'product:read']);
    }
}
